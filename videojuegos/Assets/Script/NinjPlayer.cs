using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjPlayer : MonoBehaviour
{
    private float Saltos = 10;
    private int Velocidad = 5;
    private int MaximoL = 0; 

    private const int Inactivo = 0;  //inactivo
    private const int Salta = 1; //Salta
    private const int Saltar = 2; //Saltar
    private const int Muerto = 3; //Muerto

    private Rigidbody2D rb;
    private Animator animator;

    private bool muerte = false;
    private bool correr = false;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (correr)
        {
            animator.SetInteger("Estado", Salta);
            rb.velocity = new Vector2(Velocidad, rb.velocity.y);
            //Debug.Log(vel);
            if (MaximoL == 10)
            {
                Debug.Log(Velocidad);
                Velocidad = Velocidad + 5;
                MaximoL = 0;
                Debug.Log(Velocidad);
            }
        }
        else
        {
            animator.SetInteger("Estado", Inactivo);
            rb.velocity = new Vector2(Velocidad, rb.velocity.y);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(new Vector2(rb.velocity.x, Saltos), ForceMode2D.Impulse);
            animator.SetInteger("Estado", 2);
            correr = false;
        }
        if (muerte)
        {
            animator.SetInteger("Estado", Muerto);
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Crate")
        {
            correr = true;
        }
        if (collision.gameObject.tag == "Enemy")
        {
            muerte = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "point")
        {
            MaximoL = MaximoL + 1;
            Debug.Log(MaximoL);
        }
    }

}
